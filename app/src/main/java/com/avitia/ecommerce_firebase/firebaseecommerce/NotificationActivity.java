package com.avitia.ecommerce_firebase.firebaseecommerce;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.avitia.ecommerce_firebase.firebaseecommerce.utils.firebase.Constants;

public class NotificationActivity extends AppCompatActivity {

    TextView txtTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        String body = "";
        try {
            body = getIntent().getStringExtra(Constants.NOTIFICATIONS_BODY_KEY);
        } catch (Exception e) {
            e.printStackTrace();
        }
        txtTitle = findViewById(R.id.txt_title);
        txtTitle.setText(body);

    }
}
