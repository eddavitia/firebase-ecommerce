package com.avitia.ecommerce_firebase.firebaseecommerce;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;

import com.avitia.ecommerce_firebase.firebaseecommerce.fragments.FragmentCategoriesList;
import com.avitia.ecommerce_firebase.firebaseecommerce.fragments.FragmentProductsList;
import com.avitia.ecommerce_firebase.firebaseecommerce.models.Category;
import com.avitia.ecommerce_firebase.firebaseecommerce.models.Product;
import com.avitia.ecommerce_firebase.firebaseecommerce.utils.firebase.Constants;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import static com.avitia.ecommerce_firebase.firebaseecommerce.utils.firebase.Constants.*;

public class DashBoardActivity extends BaseActivity implements
        EventListener<QuerySnapshot>,
        FragmentCategoriesList.OnCategoriesListener,
        FragmentProductsList.OnProductsListener {

    private static final String TAG = "DashBoardActivity";

    // Access a Cloud Firestore instance from your Activity
    FirebaseFirestore db = FirebaseFirestore.getInstance();

    FragmentCategoriesList fragmentCategories;
    FragmentProductsList fragmentProducts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);

        fragmentCategories = FragmentCategoriesList.newInstance();
        fragmentProducts = FragmentProductsList.newInstance();
        addOrReplaceFragment(fragmentCategories, R.id.frame_categories);
        addOrReplaceFragment(fragmentProducts, R.id.frame_products);

        fragmentCategories.setListener(this);
        fragmentProducts.setListener(this);

        db.collection(CATEGORIES_COLLECTION_NAME).addSnapshotListener(this);

    }

    /*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }
    */

    @Override
    public void onEvent(@Nullable QuerySnapshot snapshots, @Nullable FirebaseFirestoreException e) {
        if (e != null) {
            Log.w(TAG, "listen:error", e);
            return;
        }

        for (DocumentChange dc : snapshots.getDocumentChanges()) {
            switch (dc.getType()) {
                case ADDED:
                    Log.d(TAG, "New category: " + dc.getDocument().getData());
                    Category newCategory = dc.getDocument().toObject(Category.class);
                    newCategory.setId(dc.getDocument().getId());
                    fragmentCategories.addCategory(newCategory);
                    break;
                case MODIFIED:
                    Log.d(TAG, "Modified category: " + dc.getDocument().getData());
                    Category updatedCategory = dc.getDocument().toObject(Category.class);
                    fragmentCategories.updateCategory(updatedCategory);
                    break;
                case REMOVED:
                    Log.d(TAG, "Deleted category: " + dc.getDocument().getData());
                    Category deletedCategory = dc.getDocument().toObject(Category.class);
                    fragmentCategories.deleteCategory(deletedCategory);
                    break;
            }
        }

    }

    @Override
    public void onCategoriaSelected(String idCategory) {
        db.collection(PRODUCTS_COLLECTION_NAME)
                .whereEqualTo("categoryId", idCategory)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            List<Product> products = new ArrayList<>();
                            Log.d(TAG, "Documents => " + task.getResult().size());

                            for (DocumentSnapshot document : task.getResult()) {
                                Log.d(TAG, document.getId() + " => " + document.getData());
                                Product product = document.toObject(Product.class);
                                product.setId(document.getId());
                                products.add(product);
                            }
                            fragmentProducts.addProducts(products);

                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });
    }

    @Override
    public void onProductSelected(String idProduct) {
        Intent intent = new Intent(this, DetailActivity.class);
        intent.putExtra(Constants.PARAM_PRODUCT_ID, idProduct);
        startActivity(intent);
    }
}