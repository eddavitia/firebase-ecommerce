package com.avitia.ecommerce_firebase.firebaseecommerce;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by eddavitia on 3/10/18.
 */
public class BaseActivity extends AppCompatActivity {

    public void addOrReplaceFragment(Fragment fragment, int containerResId) {
        if (findViewById(containerResId) != null) {
            getSupportFragmentManager().beginTransaction().replace(containerResId, fragment).commit();
        } else {
            getSupportFragmentManager().beginTransaction().add(containerResId, fragment).commit();
        }
    }

}