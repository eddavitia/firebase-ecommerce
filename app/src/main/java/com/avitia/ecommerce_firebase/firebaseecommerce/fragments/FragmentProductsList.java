package com.avitia.ecommerce_firebase.firebaseecommerce.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.avitia.ecommerce_firebase.firebaseecommerce.R;
import com.avitia.ecommerce_firebase.firebaseecommerce.adapters.ProductsAdapter;
import com.avitia.ecommerce_firebase.firebaseecommerce.adapters.RecyclerItemClickListener;
import com.avitia.ecommerce_firebase.firebaseecommerce.models.Category;
import com.avitia.ecommerce_firebase.firebaseecommerce.models.Product;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by jlpulidol on 9/03/18.
 */
public class FragmentProductsList extends Fragment {

    private List<Product> mList;
    private OnProductsListener productsListener;
    private ProductsAdapter adapter;

    public FragmentProductsList() {
    }

    public static FragmentProductsList newInstance() {
        return new FragmentProductsList();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mList = new ArrayList<>();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_products_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        adapter = new ProductsAdapter(mList, getContext());

        RecyclerView recyclerView = view.findViewById(R.id.rv_product_list);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(adapter);
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(),
                new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        String id = mList.get(position).getId();
                        if (productsListener != null) {
                            productsListener.onProductSelected(id);
                        }
                    }
                })
        );

    }

    public void setListener(OnProductsListener listener) {
        this.productsListener = listener;
    }

    public void addProducts(List<Product> products) {
        mList.removeAll(mList);
        mList.addAll(products);
        update();
    }

    private void update() {
        adapter.notifyDataSetChanged();
    }

    public interface OnProductsListener {
        void onProductSelected(String idProduct);
    }

}