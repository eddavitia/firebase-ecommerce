package com.avitia.ecommerce_firebase.firebaseecommerce.utils.firebase;

import android.content.Intent;
import android.util.Log;

import com.avitia.ecommerce_firebase.firebaseecommerce.DashBoardActivity;
import com.avitia.ecommerce_firebase.firebaseecommerce.NotificationActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import static android.content.ContentValues.TAG;
import static com.avitia.ecommerce_firebase.firebaseecommerce.utils.firebase.Constants.*;

/**
 * Created by eddavitia on 3/10/18.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // ...

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.

        Intent intent = new Intent(this, NotificationActivity.class);
        intent.putExtra(NOTIFICATIONS_BODY_KEY, remoteMessage.getNotification().getBody());
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

    }

}
