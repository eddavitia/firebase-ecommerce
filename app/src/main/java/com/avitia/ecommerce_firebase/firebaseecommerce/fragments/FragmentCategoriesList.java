package com.avitia.ecommerce_firebase.firebaseecommerce.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.avitia.ecommerce_firebase.firebaseecommerce.R;
import com.avitia.ecommerce_firebase.firebaseecommerce.adapters.CategoriesAdapter;
import com.avitia.ecommerce_firebase.firebaseecommerce.adapters.RecyclerItemClickListener;
import com.avitia.ecommerce_firebase.firebaseecommerce.models.Category;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by jlpulidol on 9/03/18.
 */
public class FragmentCategoriesList extends Fragment {

    private List<Category> mList;
    private OnCategoriesListener listener;
    private CategoriesAdapter adapter;

    public FragmentCategoriesList() {
    }

    public static FragmentCategoriesList newInstance() {
        return new FragmentCategoriesList();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mList = new ArrayList<>();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_categories_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        adapter = new CategoriesAdapter(mList, getContext());

        RecyclerView recyclerView = view.findViewById(R.id.rv_categories_list);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        recyclerView.setAdapter(adapter);
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(),
                new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        String id = mList.get(position).getId();
                        if (listener != null) {
                            listener.onCategoriaSelected(id);
                        }
                    }
                })
        );
    }

    public void setListener(OnCategoriesListener listener){
        this.listener = listener;
    }

    public void addCategory(Category newCategory) {
        mList.add(newCategory);
        update();
    }

    public void updateCategory(Category category) {
        int i = 0;
        for (Category c : mList) {
            if (c.getName().equals(category.getName())) {
                mList.get(i).setIndex(category.getIndex());
                c = category;
            }
            i++;
        }
        update();
    }

    public void deleteCategory(Category category) {
        for (Category c : mList) {
            if (c.getName().equals(category.getName())) {
                mList.remove(c);
            }
        }
        update();
    }

    private void update() {
        Collections.sort(mList, new Comparator<Category>() {
            @Override
            public int compare(Category previousCat, Category newCat) {
                // -1 - less than, 1 - greater than, 0 - equal, all inversed for descending
                return previousCat.getIndex() > newCat.getIndex() ? -1 : (previousCat.getIndex() < newCat.getIndex()) ? 1 : 0;
            }
        });
        adapter.notifyDataSetChanged();
    }

    public interface OnCategoriesListener {
        void onCategoriaSelected(String nameCategory);
    }

}