package com.avitia.ecommerce_firebase.firebaseecommerce.utils.firebase;

/**
 * Created by eddavitia on 3/9/18.
 */
public class Constants {

    // Remote Config keys
    public static final String PROMOCION_SANVALENTIN_ACTIVA_KEY = "promocion_sanvalentin_activa";
    public static final String PROMOCION_NOCTURNA_HORA_KEY = "promocion_nocturna_hora";
    public static final String PROMOCION_CLIMA_GRADOS_KEY = "promocion_clima_grados";

    // Collections, Documents keys
    public static final String CATEGORIES_COLLECTION_NAME = "categories";
    public static final String PRODUCTS_COLLECTION_NAME = "products";
    public static final String ORDERS_COLLECTION_NAME = "orders";

    public static final String PURCHASED_PRODUCT_ID_KEY = "purchasedProductId";


    //Notifications
    public static final String NOTIFICATIONS_BODY_KEY = "notifications_body_key";
    public static final String PARAM_PRODUCT_ID = "product_id";

    //Events
    public static final String BUY_PRODUCT_EVENT_NAME = "buy_product";

    //user properties
    public static final String USER_PROPERTY_EXAMPLE = "favorite_food";


}