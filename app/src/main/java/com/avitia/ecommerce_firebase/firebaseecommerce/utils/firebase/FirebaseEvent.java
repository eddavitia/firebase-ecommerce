package com.avitia.ecommerce_firebase.firebaseecommerce.utils.firebase;

import android.content.Context;
import android.os.Bundle;

import com.google.firebase.analytics.FirebaseAnalytics;

/**
 * Created by jlpulidol on 9/03/18.
 */

public enum FirebaseEvent {

    OPEN_APP{
        @Override
        public String getName() {
            return "open_app";
        }

        @Override
        public Bundle getParams() {
            Bundle params = new Bundle();
            return params;
        }
    },

    BUY_PRODUCT{
        @Override
        public String getName() {
            return "buy_product";
        }

        @Override
        public Bundle getParams() {
            Bundle params = new Bundle();
            return params;
        }
    };

    public abstract String getName();

    public abstract Bundle getParams();

    public static void logEvent(FirebaseEvent event, Context context, Bundle customParams) {
        try {
            FirebaseAnalytics.getInstance(context).logEvent(
                    event.getName(),
                    customParams != null ? customParams : event.getParams());
        } catch (NullPointerException ex) {
            ex.printStackTrace();
        }
    }

    public static void logEvent(FirebaseEvent event, Context context) {
        try {
            FirebaseAnalytics.getInstance(context).logEvent(
                    event.getName(),
                    event.getParams());
        } catch (NullPointerException ex) {
            ex.printStackTrace();
        }
    }
}
