package com.avitia.ecommerce_firebase.firebaseecommerce.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.avitia.ecommerce_firebase.firebaseecommerce.R;
import com.avitia.ecommerce_firebase.firebaseecommerce.models.Category;
import com.bumptech.glide.Glide;

import java.util.List;

/**
 * Created by jlpulidol on 9/03/18.
 */
public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.CategoryViewHolder> {

    private List<Category> categories;
    private Context context;
    private int lastPosition = -1;

    public CategoriesAdapter(List<Category> categories, Context context) {
        this.categories = categories;
        this.context = context;
    }

    @Override
    public CategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.item_category, parent, false);
        return new CategoryViewHolder(v);
    }

    @Override
    public void onBindViewHolder(CategoryViewHolder holder, int position) {
        final Category categoria = categories.get(position);
        String categoryName = categoria.getName();
        categoryName = categoryName.replaceAll(" ", "");
        holder.txtNombre.setText(categoryName);
        String imagenUrl = categoria.getImageUrl();
        Glide.with(context).load(imagenUrl).into(holder.imgCategory);

        // Here you apply the animation when the view is bound
        setAnimation(holder.itemView, position);

    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    class CategoryViewHolder extends RecyclerView.ViewHolder {
        ImageView imgCategory;
        TextView txtNombre;

        CategoryViewHolder(View itemView) {
            super(itemView);
            imgCategory = itemView.findViewById(R.id.image_item_product_category_image);
            txtNombre = itemView.findViewById(R.id.text_item_product_category_name);
        }
    }

    /**
     * Here is the key method to apply the animation
     */
    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

}