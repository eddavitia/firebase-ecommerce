package com.avitia.ecommerce_firebase.firebaseecommerce.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.avitia.ecommerce_firebase.firebaseecommerce.R;
import com.avitia.ecommerce_firebase.firebaseecommerce.models.Category;
import com.avitia.ecommerce_firebase.firebaseecommerce.models.Product;
import com.bumptech.glide.Glide;

import java.util.List;

/**
 * Created by jlpulidol on 9/03/18.
 */
public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ProductsViewHolder> {

    public List<Product> products;
    private Context context;
    private int lastPosition = -1;

    public ProductsAdapter(List<Product> productos, Context context) {
        this.products = productos;
        this.context = context;
    }

    @Override
    public ProductsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.item_product, parent, false);
        return new ProductsViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ProductsViewHolder holder, int position) {
        Product producto = products.get(position);
        String productName = producto.getName();

        holder.txtNombreProducto.setText(productName);
        holder.txtProductPrice.setText(String.valueOf(producto.getPrice()));
        String imagenUrl = producto.getImageUrl();
        Glide.with(context).load(imagenUrl).into(holder.imgProducto);

        // Here you apply the animation when the view is bound
        setAnimation(holder.itemView, position);
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    public static class ProductsViewHolder extends RecyclerView.ViewHolder {
        ImageView imgProducto;
        TextView txtNombreProducto;
        TextView txtProductPrice;

        ProductsViewHolder(View itemView) {
            super(itemView);
            imgProducto = itemView.findViewById(R.id.image_item_product_image);
            txtNombreProducto = itemView.findViewById(R.id.text_item_product_name);
            txtProductPrice = itemView.findViewById(R.id.text_item_product_price);
        }
    }

    /**
     * Here is the key method to apply the animation
     */
    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.fade_in);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

}