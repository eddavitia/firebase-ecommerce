package com.avitia.ecommerce_firebase.firebaseecommerce;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.avitia.ecommerce_firebase.firebaseecommerce.utils.firebase.Constants;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

import static com.avitia.ecommerce_firebase.firebaseecommerce.utils.firebase.Constants.*;

public class DetailActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "DetailActivity";
    Button btnComprar;
    String productId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);


        try {
            productId = getIntent().getExtras().getString(Constants.PARAM_PRODUCT_ID);
        } catch (Exception e) {
            e.printStackTrace();
        }

        btnComprar = findViewById(R.id.btn_buy);
        btnComprar.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {

        if (productId != null && productId.isEmpty()) {
            Toast.makeText(this, "no existe ese producto", Toast.LENGTH_LONG).show();
            return;
        }

        Bundle bundle = new Bundle();
        bundle.putString(PARAM_PRODUCT_ID, productId);
        FirebaseAnalytics.getInstance(this).logEvent(BUY_PRODUCT_EVENT_NAME, bundle);

        FirebaseAnalytics.getInstance(this).setUserProperty(USER_PROPERTY_EXAMPLE, "apples");


        // Otra opción

        // Access a Cloud Firestore instance from your Activity
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        Map<String, Object> order = new HashMap<>();
        order.put(Constants.PURCHASED_PRODUCT_ID_KEY, productId);

        db.collection(Constants.ORDERS_COLLECTION_NAME)
                .add(order)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Log.d(TAG, "DocumentSnapshot written with ID: " + documentReference.getId());
                        goToBuy();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error adding document", e);
                        Toast.makeText(getApplicationContext(), "¡Este producto se ha agotado", Toast.LENGTH_SHORT).show();
                    }
                });

    }

    private void goToBuy(){
        Toast.makeText(this, "¡Gracias por comprar este producto!", Toast.LENGTH_LONG).show();
        finish();
    }

}