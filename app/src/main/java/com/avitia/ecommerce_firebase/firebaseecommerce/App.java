package com.avitia.ecommerce_firebase.firebaseecommerce;

import android.app.Application;

import io.realm.Realm;

/**
 * Created by eddavitia on 3/10/18.
 */
public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
    }
}